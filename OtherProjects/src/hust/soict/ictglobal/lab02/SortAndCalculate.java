package hust.soict.ictglobal.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class SortAndCalculate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arrEg = {4,7,8,5,-2,11,-6,20};
		
		System.out.println("Array: " + Arrays.toString(arrEg));
		Arrays.sort(arrEg);
		System.out.println("Sorted: " + Arrays.toString(arrEg));
		System.out.println("Sum: " + Arrays.stream(arrEg).sum());
		System.out.println("Avarage: " + Arrays.stream(arrEg).average());
		
		// -----------------------------------------
		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nSize: "); 
		int n = sc.nextInt();
		System.out.println("Enter Array: "); 
		int[] a = new int[n];
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}
		sc.close();
		
		System.out.println("Array: " + Arrays.toString(a));
		Arrays.sort(a);
		System.out.println("Sorted: " + Arrays.toString(a));
		System.out.println("Sum: " + Arrays.stream(a).sum());
		System.out.println("Avarage: " + Arrays.stream(a).average());
		
	}

}
