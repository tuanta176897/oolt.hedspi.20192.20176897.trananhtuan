package hust.soict.ictglobal.aims.media;

public class BookTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Book book = new Book("Lorem ipsum", "Sample", 0.01f);
		book.addAuthor("One");
		book.addAuthor("Two");
		book.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Curabitur aliquet quam id dui posuere blandit.");
		book.processContent();
		
		System.out.println(book);
	}

}
