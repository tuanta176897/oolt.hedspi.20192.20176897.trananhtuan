package hust.soict.ictglobal.date;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {
	public static final String MY_DATE_FORMAT_1 = "dd/MM/yyyy";
	public static final String MY_DATE_FORMAT_2 = "dd-MM-yyyy";
	
	private int day, month, year;

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (day > 0 && day <= 31)
			this.day = day;
		else {
			System.out.println("Invalid day");
			System.exit(0);
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month >= 1 && month <= 12)
			this.month = month;
		else {
			System.out.println("Invalid month");
			System.exit(0);
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (year > 0)
			this.year = year;
		else {
			System.out.println("Invalid year");
			System.exit(0);
		}
	}

	public MyDate() {
		super();
		LocalDate today = LocalDate.now();
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}

	public MyDate(int day, int month, int year) {
		super();
		this.setDay(day);
		this.setMonth(month);
		this.setYear(year);
	}

	public MyDate(String strDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d uuuu");
		LocalDate today = LocalDate.parse(strDate, formatter);
		this.day = today.getDayOfMonth();
		this.month = today.getMonthValue();
		this.year = today.getYear();
	}

	public void accept() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a Date (e.g: Dec 3 2011): ");

		String strDate = sc.nextLine();

		sc.close();

		MyDate newDate = new MyDate(strDate);
		this.day = newDate.getDay();
		this.month = newDate.getMonth();
		this.year = newDate.getYear();
	}

	public void print() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d uuuu");
		String today = LocalDate.of(this.year, this.month, this.day).format(formatter);
		System.out.println("Today is " + today);
	}

	// lab04
	public void setX(String x) {
		try {
			Month m = Month.valueOf(x.toUpperCase());
			this.month = m.getValue();
		} catch (IllegalArgumentException e) {
			if (x.length() <= "twenty eight".length())
				System.out.println("Day la ngay");
			else 
				System.out.println("Day la nam");
		}
	}

	public void print2(String style) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(style);
		String date = LocalDate.of(this.year, this.month, this.day).format(formatter);
		System.out.print(date);
	}
	
}
