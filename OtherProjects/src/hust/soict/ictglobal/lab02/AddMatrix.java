package hust.soict.ictglobal.lab02;

import java.util.Arrays;
import java.util.Scanner;

public class AddMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i ,j;
		int aEg[][] = {
				{1,2,3,4},
				{4,3,2,1},
				{1,2,3,4}
		};
		int bEg[][] = {
				{4,3,2,1},
				{1,2,3,4},
				{4,3,2,1}
		};
		int cEg[][] = new int[3][4];
		for (i = 0; i < 3; i++) {
			for (j = 0 ; j < 4; j++) {
				cEg[i][j] = aEg[i][j] + bEg[i][j];
			}
		}
		System.out.println("Answer: " + Arrays.deepToString(cEg));
		// ------------------------------------
		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nSize (n m): "); 
		int n = sc.nextInt(); int m = sc.nextInt();
		System.out.println("Enter Matrix A(" + n + "x" + m + "):"); 
		int[][] a = new int[n][m];
		for (i = 0; i < n; i++)
			for (j = 0; j < m; j++) 
				a[i][j] = sc.nextInt();
		System.out.println("A: " + Arrays.deepToString(a));
		//
		System.out.println("Enter Matrix B(" + n + "x" + m + "):"); 
		int[][] b = new int[n][m];
		for (i = 0; i < n; i++) 
			for (j = 0; j < m; j++) 
				b[i][j] = sc.nextInt();
		sc.close();
		//
		int c[][] = new int[n][m];
		for (i = 0; i < n; i++) {
			for (j = 0 ; j < m; j++) {
				c[i][j] = a[i][j] + b[i][j];
			}
		}
		System.out.println("B: " + Arrays.deepToString(b));
		
		System.out.println("Answer: " + Arrays.deepToString(c));
	}

}
