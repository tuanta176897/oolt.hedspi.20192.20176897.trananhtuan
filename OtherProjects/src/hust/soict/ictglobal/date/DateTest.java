package hust.soict.ictglobal.date;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("-------------Test code lab03");
		// test MyDate
		MyDate newDate = new MyDate();
		newDate.print();

		MyDate newDate2 = new MyDate(30, 11, 2019);
		newDate2.print();

//		newDate.accept();
//		newDate.print();

		System.out.println("-------------Test code lab04");
		newDate.setX("December");
		newDate.print2(MyDate.MY_DATE_FORMAT_1);
		System.out.println();
		
		// test DateUtils
		DateUtils utils = new DateUtils();
		MyDate date1 = new MyDate(11, 12, 1999);
		MyDate date2 = new MyDate(11, 11, 1999);
		MyDate date3 = new MyDate(31, 1, 2019);
		MyDate date4 = new MyDate(1, 5, 2012);
		
		date1.print2(MyDate.MY_DATE_FORMAT_1);
		System.out.print(utils.compare(date1, date2) > 0 ? " is after " : " is before ");
		date2.print2(MyDate.MY_DATE_FORMAT_2);
		System.out.println();
		
		MyDate[] datesList = {date1, date2, date3, date4};
		utils.sort(datesList);
		
		System.out.println("After sorting: ");
		for (int i = 0; i < datesList.length; i++) {
				datesList[i].print2(MyDate.MY_DATE_FORMAT_1);
				System.out.println();
		}
	}

}
