package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class DaysOfMonth {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
							  //   1  2  3  4  5  6  7  8  9  10 11 12  
		final int[] daysOfMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
		
		System.out.print("Month: "); 
		int month = sc.nextInt();
		while (month < 1 || month > 12) {
			System.out.println("Invalid month, please try again!");
			System.out.print("Month: "); 
			month = sc.nextInt();
		}
		
		System.out.print("Year: "); 
		int year = sc.nextInt();
		while (year < 1 || year > 9999) {
			System.out.println("Invalid year, please try again!");
			System.out.print("Year: "); 
			year = sc.nextInt();
		}
		sc.close();
		
		if (month == 2 && (year % 4 == 0))
			System.out.println("Days of month: 29");
		else
			System.out.println("Days of month: " + daysOfMonth[month-1]);
	}

}
