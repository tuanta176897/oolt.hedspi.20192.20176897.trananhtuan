package hust.soict.ictglobal.lab01;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class EquationSolver {
	static void firstDeg1Var() {
		JTextField sa = new JTextField();
		JTextField sb = new JTextField();
		Object[] message = {
				"a: ", sa, 
				"b: ", sb
		};
		JOptionPane.showConfirmDialog(null, message, "INPUT", JOptionPane.DEFAULT_OPTION);
		JOptionPane.showMessageDialog(null, "Answer: x = " + 
				((-1)*Double.parseDouble(sb.getText())/Double.parseDouble(sa.getText())));
	}
	
	static void firstDeg2Var() {
		JTextField sa = new JTextField();
		JTextField sb = new JTextField();
		JTextField sc = new JTextField();
		Object[] message = {
				"a: ", sa, 
				"b: ", sb,
				"c: ", sc
		};
		JOptionPane.showConfirmDialog(null, message, "INPUT", JOptionPane.DEFAULT_OPTION);
		JOptionPane.showMessageDialog(null, 
				"Answer: x = " + sc.getText() + " - " + sb.getText() + "y/" + sa.getText());
	}
	
	static void secondDeg1Var() {
		JTextField sa = new JTextField();
		JTextField sb = new JTextField();
		JTextField sc = new JTextField();
		Object[] message = {
				"a: ", sa, 
				"b: ", sb,
				"c: ", sc
		};
		// input
		JOptionPane.showConfirmDialog(null, message, "INPUT", JOptionPane.DEFAULT_OPTION);
		// calculate
		double a = Double.parseDouble(sa.getText());
		double b = Double.parseDouble(sb.getText());
		double c = Double.parseDouble(sc.getText());
		double delta = b*b - 4*a*c;

		if (delta < 0) 
			JOptionPane.showMessageDialog(null, "Phuong trinh vo nghiem", "OUTPUT", JOptionPane.WARNING_MESSAGE);
		else if (delta == 0)
			JOptionPane.showMessageDialog(null, "Phuong trinh co 1 nghiem kep : x = " + -b/(2*a));
		else 
			JOptionPane.showMessageDialog(null, "Phuong trinh co 2 nghiem:\nx1 = " +
					(-b+Math.sqrt(delta))/(2*a) + "va x2 = " +
					(-b-Math.sqrt(delta))/(2*a));
	}
	
//----------------------------------------------
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] options = {"ax + b = 0", "ax+by=c", "ax^2+bx+c=0"};
		
		int chosen = JOptionPane.showOptionDialog(null, "Please choose your option: ", "Option", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);

		switch (chosen) {
		case 0: firstDeg1Var(); break;
		case 1: firstDeg2Var(); break;
		case 2: secondDeg1Var(); break;
		}
		
		System.exit(0);
	}

}
