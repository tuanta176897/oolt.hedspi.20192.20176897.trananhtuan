package hust.soict.ictglobal.aims.disc;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// test search
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("Harry Aladin Potter");
		System.out.println(dvd1.search("Harry Potter"));
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Harry Aladin");
		System.out.println(dvd2.search("Harry Potter"));
		
		// test lucky item
		Order anOrder = new Order();
		dvd1.setCost(10);
		dvd2.setCost(11);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Pokemon", "", "", 12, 12);
		anOrder.addMedia(dvd1);
		anOrder.addMedia(dvd2);
		anOrder.addMedia(dvd3);		
		
		Media luckyItem = anOrder.getALuckyItem();
		
		anOrder.print();
	}

}
