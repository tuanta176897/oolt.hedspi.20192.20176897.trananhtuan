package hust.soict.ictglobal.lab01;

import javax.swing.JOptionPane;

public class SimpleCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String strNum1, strNum2, strOp;
		double num1, num2, ans;
		char op;
		String strNotification = "Answer: ";
		
		strNum1 = JOptionPane.showInputDialog(null,
				"Please input the first number: ", "Input the first number",
				JOptionPane.INFORMATION_MESSAGE);
		num1 = Double.parseDouble(strNum1); //them error
		
		strNum2 = JOptionPane.showInputDialog(null,
				"Please input the second number: ", "Input the second number",
				JOptionPane.INFORMATION_MESSAGE);
		num2 = Double.parseDouble(strNum2); //them error
		
		strOp = JOptionPane.showInputDialog(null, 
				"Plase input the operator: ", "Input operator",
				JOptionPane.INFORMATION_MESSAGE);
		op = strOp.charAt(0); //them error
		switch (op) {
		case '+': ans = num1 + num2; break;
		case '-': ans = num1 - num2; break;
		case '*': ans = num1 * num2; break;
		case '/': ans = num1 / num2; break;
		default: 
			ans = 0;
			JOptionPane.showMessageDialog(null, "Invalid Operator, please try again!", "Invalid Operator", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
		strNotification += strNum1 + " " + op + " " + strNum2 + " = " + ans;
		JOptionPane.showMessageDialog(null, strNotification, 
				"Answer", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}

}
