package hust.soict.ictglobal.aims.order;

import java.time.LocalDate;
import java.util.ArrayList;

import hust.soict.ictglobal.aims.media.Media;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 3;
	public static final int MAX_LIMITTED_ORDERS = 5;

	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	private String dateOrdered;
	private static int nbOrders = 0;
	
	public Order() {
		super();
		if (nbOrders < MAX_LIMITTED_ORDERS) {
			nbOrders++;
			dateOrdered = LocalDate.now().toString();
			System.out.println("Order has been created");
		} else {
			System.out.println("Limited orders");
			System.exit(0);
		}
	}

	public String getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(String dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public void addMedia(Media media) {
		if (itemsOrdered.size() < MAX_NUMBERS_ORDERED) {
			itemsOrdered.add(media);
			System.out.println("The media has been added");
		} else
			System.out.println("The order is almost full");
	}

	public void removeMedia(Media media) {
		if (this.itemsOrdered.indexOf(media) < 0) 
			System.out.println("Not present");
		else {
			this.itemsOrdered.remove(media);
			System.out.println("The media has been removed");
		}
	}

	// FIXME: make this shorten
	public void removeMedia(int id) {
		boolean present = false;
		int i;
		for (i = 0; i < this.itemsOrdered.size(); i++) 
			if (itemsOrdered.get(i).getId() == id) {
				present = true;
				break;
			}
		if (!present) 
			System.out.println("Not present");
		else {
		this.itemsOrdered.remove(i);
		System.out.println("The media has been removed");
		}
	}
	
	public float totalCost() {
		float sum = 0;
		for (int i = 0; i < itemsOrdered.size(); i++) {
			sum += itemsOrdered.get(i).getCost();
		}
		return sum;
	}

	// lab04
	public void addMedia(Media[] mediaList) {
		int i;

		for (i = 0; i < mediaList.length; i++) {
			if (itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
				System.out.println("Full ordered items");
				break;
			}
			this.addMedia(mediaList[i]);
		}
	}

	public void addMedia(Media media1, Media media2) {
		if (itemsOrdered.size()  == MAX_NUMBERS_ORDERED) {
			System.out.print("Order is full. ");
			System.out.println(media1.getTitle() + " and " + media2.getTitle() + 
					" couldn't be added");
		}
		else if (itemsOrdered.size()  == MAX_NUMBERS_ORDERED-1) {
			this.addMedia(media1);
			System.out.println(media2.getTitle() + " couldn't be added");
		}
		else {
			this.addMedia(media1);
			this.addMedia(media2);
		}
	}
	
	public void print() {
		System.out.println("****************Order****************");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered Items:");
		for (int i = 0; i < itemsOrdered.size(); i++) 
			System.out.println(this.itemsOrdered.get(i).getId() + ". " + 
					this.itemsOrdered.get(i).getTitle() + " - " +
//					((DigitalVideoDisc)this.itemsOrdered.get(i)).getLength() + ": " +
					this.itemsOrdered.get(i).getCost() + "$");
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("*************************************");
	}
	
	public Media getALuckyItem() {
		int luckyNumber = (int)(Math.random() * (MAX_NUMBERS_ORDERED-1));
		this.itemsOrdered.get(luckyNumber).setCost(0);
		String newTitle = this.itemsOrdered.get(luckyNumber).getTitle() + " (Free Item)";
		this.itemsOrdered.get(luckyNumber).setTitle(newTitle);
		return this.itemsOrdered.get(luckyNumber);
	}
	
}