package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<CompactDisc> {
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	// FIXME: make this shorten
	public int getLength() {
		int sum = 0;
		for (int i = 0; i < this.tracks.size(); i++) {
			sum+= this.tracks.get(i).getLength();
		}
		return sum;
	}

	public ArrayList<Track> getTracks() {
		return tracks;
	}

	public CompactDisc(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public void addTrack(Track newTrack) {
		if (this.tracks.indexOf(newTrack) >= 0)
			System.out.println("Already in the list");
		else 
			this.tracks.add(newTrack);
	}
	
	public void removeTrack(Track delTrack) {
		int i;
		if ((i = this.tracks.indexOf(delTrack)) >= 0)
			this.tracks.remove(i);
		else
			System.out.println("Not present");
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		for (int i = 0; i < this.tracks.size(); i++) {
			this.tracks.get(i).play();
		}
	}

	@Override
	public int compareTo(CompactDisc o) {
		// TODO Auto-generated method stub
		// sort by title
//		return this.getTitle().compareTo(o.getTitle());
		// sort by number of tracks
		if(this.tracks.size() == o.getTracks().size()) {			
			return Integer.compare(this.getLength(), o.getLength());
		}													
		return Integer.compare(this.tracks.size(), o.getTracks().size());
	}
	
	
}
