package hust.soict.ictglobal.aims.media;

public class Disc extends Media{
	private int length;
	private String director;
	
	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public Disc(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title, String category, float cost, int id) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	
}
