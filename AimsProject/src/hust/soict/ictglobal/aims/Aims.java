package hust.soict.ictglobal.aims;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
//import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.MemoryDaemon;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

public class Aims {
	
	private static Scanner sc = new Scanner(System.in);
	private static Order anOrder;
	
	private static void showMenu() {
		System.out.println("Order Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}
	
	private static void addItem() {
		String answer;
		
		System.out.println("Please choose a type(1-2-3): ");
		System.out.println("1. Book");
		System.out.println("2. CompactDisc");
		System.out.println("3. DigitalVideoDisc");
		System.out.println("--------------------------------");
		
		int n = sc.nextInt();
		sc.nextLine();
		
		switch (n) {
		case 1:
			// test
			Book book = new Book(Math.abs(LocalDateTime.now().hashCode()%100)) ;
			book.setTitle("New Book");
			book.setCategory("Category");
			book.setCost(100);
			
			ArrayList<String> authors = new ArrayList<String>();
			authors.add("Authors 1");
			authors.add("Authors 2");
			book.setAuthors(authors);
			
			anOrder.addMedia(book);
			break;
		case 2:
			// test
			CompactDisc cd = new CompactDisc(Math.abs(LocalDateTime.now().hashCode()%100));
			cd.setTitle("New CD");
			cd.setCategory("Category");
			cd.setCost(100);
			cd.setArtist("Artist");
			
			Track track1 = new Track();
			track1.setTitle("Title 1");
			track1.setLength(10);
			cd.addTrack(track1);
			
			anOrder.addMedia(cd);
			
			System.out.println("Do you want to play this track?(Yes/No)");
			answer = sc.next();
			if(answer.equals( "yes"))
				cd.play();
			
			break;
		case 3:
			// test
			DigitalVideoDisc dvd = new DigitalVideoDisc(Math.abs(LocalDateTime.now().hashCode()%100));
			dvd.setTitle("New DVD");
			dvd.setCategory("Category");
			dvd.setCost(100);
			dvd.setDirector("Director");
			dvd.setLength(0);
			
			anOrder.addMedia(dvd);
			
			System.out.println("Do you want to play this track?(Yes/No)");
			answer = sc.next();
			if(answer.equals( "yes"))
				dvd.play();
			
			break;
		default:
			System.out.println("Invalid Answer");
			break;
		}
		
//		test input from keyboard
//		Media newItem = new Media();
//		System.out.println("Please enter following informations");
//		System.out.print("Title: ");
//		newItem.setTitle(sc.nextLine());
//		System.out.print("Category: ");
//		newItem.setCategory(sc.nextLine());
//		System.out.print("Cost: ");
//		newItem.setCost(sc.nextFloat());
//		sc.nextLine();
//		
//		anOrder.addMedia(newItem);
	}

	public static void removeItem() {
		System.out.print("Enter the id: ");
		anOrder.removeMedia(sc.nextInt());
		sc.nextLine();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MemoryDaemon md = new MemoryDaemon();
		Thread dt = new Thread(md);
		dt.setDaemon(true);
		dt.start();
		
		int n;
		
//		while (true) {
//			showMenu();
//			n = sc.nextInt();
//			sc.nextLine();
//			switch (n) {
//			case 1:
//				anOrder = new Order();
//				break;
//			case 2:
//				addItem();
//				break;
//			case 3:
//				removeItem();
//				break;
//			case 4:
//				anOrder.print();
//				break;
//			case 0:
//				sc.close();
//				System.exit(0);
//			default:
//				break;
//			}
//		}
		
		// lab08
		// Add the DVD objects to the ArrayList
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", "Roger Allers", 87, 19.95f);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", "George Lucas", 124, 24.95f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f);
		
		List<DigitalVideoDisc> discs = new ArrayList<DigitalVideoDisc>();
		discs.add(dvd2);
		discs.add(dvd1);
		discs.add(dvd3);
		
		// Interator through the ArrayList and output their titles
		// (unsorted order)
		Iterator<DigitalVideoDisc> iterator = discs.iterator();
		
		System.out.println("------------------------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		// Sort the collection of DVDs - based on the compareTo() method
		Collections.sort(discs);
		
		// Iterate through the ArrayList and output their titles - in sorted order
		iterator = discs.iterator();
		System.out.println("------------------------------------");
		System.out.println("The DVDs in sorted order are: ");
	
		while(iterator.hasNext()) {
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		System.out.println("------------------------------------");
	}
	
}
