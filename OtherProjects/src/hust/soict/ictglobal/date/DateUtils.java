package hust.soict.ictglobal.date;

import java.time.LocalDate;

public class DateUtils {

	public int compare(MyDate date1, MyDate date2) {
		LocalDate d1 = LocalDate.of(date1.getYear(), date1.getMonth(), date1.getDay());
		LocalDate d2 = LocalDate.of(date2.getYear(), date2.getMonth(), date2.getDay());
		return d1.compareTo(d2);
	}

	public static void sort(MyDate[] datesList) {
		for (int i = 0; i < datesList.length - 1; i++) {
			for (int j = i + 1; j < datesList.length; j++) {
				if (new DateUtils().compare(datesList[i], datesList[j]) > 0) {
					dateSwap(datesList[i], datesList[j]);
				}
			}
		}
	}

	public static void dateSwap(MyDate d1, MyDate d2) {
		MyDate tmp = new MyDate(d1.getDay(), d1.getMonth(), d1.getYear());
		
		d1.setDay(d2.getDay());
		d1.setMonth(d2.getMonth());
		d1.setYear(d2.getYear());
		
		d2.setDay(tmp.getDay());
		d2.setMonth(tmp.getMonth());
		d2.setYear(tmp.getYear());
	}
}
