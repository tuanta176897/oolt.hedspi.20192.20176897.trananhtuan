package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Book extends Media implements Comparable<Book> {

	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrquency;
	
	public Book(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}
	
	public Book(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public Book(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public Book(String title, String category, float cost, int id, List<String> authors) {
		super(title, category, cost, id);
		// TODO Auto-generated constructor stub
		this.authors = authors;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(ArrayList<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		if (this.authors.indexOf(authorName) >= 0)
			System.out.println("Already in the list");
		else 
			this.authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		int i;
		if ((i = this.authors.indexOf(authorName)) >= 0)
			this.authors.remove(i);
		else
			System.out.println("Not present");
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public int compareTo(Book o) {
		// TODO Auto-generated method stub
		return this.getTitle().compareTo(o.getTitle());
	}
	
	public void processContent() {
	    String reg = "[, ?.@]+";

	    String[] tokens = this.content.split(reg);
	    for (int i = 0; i < tokens.length; i++) {
			this.contentTokens.add(tokens[i]);
		}
	    
	    Collections.sort(this.contentTokens);
	    
	    this.wordFrquency = new HashMap<String, Integer>();
	    Iterator<String> iterator = this.contentTokens.iterator();
	    
	    while (iterator.hasNext()) {
	    	String key = iterator.next();
	    	if(this.wordFrquency.containsKey(key)) {
	    		this.wordFrquency.put(key, this.wordFrquency.get(key) + 1);
			}
			else {
				this.wordFrquency.put(key, 1); 
			}
	    }
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String info = this.getTitle() + "\n"
					+ this.getCategory() + "\n"
					+ this.getAuthors().toString() + "\n"
					+ Float.toString(this.getCost()) + "\n"
					+ this.getContent() + "\n"
					+ Integer.toString(this.contentTokens.size()) + "\n";
		
		Iterator<String> iterator = this.contentTokens.iterator();
		
		while(iterator.hasNext()) {
			info = info + iterator.next() + " ";
		}
		info = info + "\n";
		for(Map.Entry<String, Integer> entry: this.wordFrquency.entrySet()) {
			info = info + entry.getKey() + ": " + Integer.toString(entry.getValue()) + " ";
		}
		
		return info;
	}
	
	
}
