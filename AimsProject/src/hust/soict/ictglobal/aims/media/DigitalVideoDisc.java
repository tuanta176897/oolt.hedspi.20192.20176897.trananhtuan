package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<DigitalVideoDisc> {

	private String director;
	private int length;
	
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public DigitalVideoDisc(int id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	public DigitalVideoDisc(String title) {
		super(title);
	}

	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}

	public DigitalVideoDisc(String title, String category, String director) {
		super(title, category);
		this.director = director;
	}

	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	
	public boolean search(String title) {
		String[] tokens = title.split(" ");
		for (int i = 0; i < tokens.length; i ++) {
			if (!this.title.contains(tokens[i]))
				return false;
		}
		return true;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}

	@Override
	public int compareTo(DigitalVideoDisc o) {
		// TODO Auto-generated method stub
		// sort by title
//		return this.getTitle().compareTo(o.getTitle());
		// sort by cost
		return Double.compare(this.getCost(), o.getCost());
	}
	
	
}
